﻿using Demo_ManageRestauran.Models;
using Demo_ManageRestauran.Reponsitory.FoodReponsitory;
using Demo_ManageRestauran.Reponsitory.FoodRestaurentReponsitory;
using Demo_ManageRestauran.Reponsitory.GenericReponsitory;
using Demo_ManageRestauran.Reponsitory.RestauranReponsitory;
using Demo_ManageRestauran.Service.FoodRestaurentService;
using Demo_ManageRestauran.Service.FoodService;
using Demo_ManageRestauran.Service.RestauranReponsitory;
using Demo_ManageRestauran.UnitOfWork;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Demo_ManageRestauran
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<DBContext>
            (
                options =>
                    options.UseSqlServer(
                        Configuration.GetConnectionString("ConnectDB")
                    )
            );
            services.AddTransient<IGenericReponsitory<Food>, GenericReponsitory<Food>>();
            services.AddTransient<IFoodService, FoodService>();
            services.AddTransient<IFoodReponsitory, FoodReponsitory>();
            
            services.AddTransient<IGenericReponsitory<Restauran>, GenericReponsitory<Restauran>>();
            services.AddTransient<IRestauranSerrvice, RestauranService>();
            services.AddTransient<IRestauranReponsitory, RestauranReponsitory>();
            
            services.AddTransient<IGenericReponsitory<FoodRestaurent>, GenericReponsitory<FoodRestaurent>>();
            services.AddScoped<IFoodRestaurantService, FoodRestaurantService>();
            services.AddTransient<IFoodRestaurentReponsitory, FoodRestaurentReponsitory>();
            
            services.AddTransient<IUnitOfWork, UnitOfWork.UnitOfWork>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}