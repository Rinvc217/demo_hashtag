﻿using System;
using Demo_ManageRestauran.Models;
using Demo_ManageRestauran.Reponsitory.GenericReponsitory;

namespace Demo_ManageRestauran.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericReponsitory<Food> FoodReponsitory { get; }
        IGenericReponsitory<Restauran> RestauranReponsitory { get; }
        IGenericReponsitory<FoodRestaurent> FoodRestaurentReponsitory { get; }
        void save();
    }
}