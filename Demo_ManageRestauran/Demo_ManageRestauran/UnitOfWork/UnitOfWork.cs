﻿using System;
using Demo_ManageRestauran.Models;
using Demo_ManageRestauran.Reponsitory.GenericReponsitory;

namespace Demo_ManageRestauran.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private DBContext _context;


        public UnitOfWork(DBContext context)
        {
            _context = context;
            InitReponsitory();
        }

        private void InitReponsitory()
        {
            FoodReponsitory = new GenericReponsitory<Food>(_context);
            RestauranReponsitory = new GenericReponsitory<Restauran>(_context);
        }

        private bool _disposed = false;
        private IUnitOfWork _unitOfWork;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public IGenericReponsitory<Food> FoodReponsitory { get; set; }
        public IGenericReponsitory<Restauran> RestauranReponsitory { get; set; }
        public IGenericReponsitory<FoodRestaurent> FoodRestaurentReponsitory { get; }

        public void save()
        {
            _context.SaveChanges();
        }
    }
}