﻿using Microsoft.EntityFrameworkCore;


namespace Demo_ManageRestauran.Models
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Food>(entity =>
            {
                entity.HasKey(e => e.FoodID);
                entity.Property(e => e.FoodID).HasColumnName("FoodID");
                entity.Property(e => e.FoodName).HasColumnName("FoodName");
                entity.Property(e => e.Description).HasColumnName("Description");
                entity.Property(e => e.Price).HasColumnName("Price");
            });
            //Sử dụng phương thức HasKey () để định cấu hình tên của ràng buộc khóa chính trong cơ sở dữ liệu.
            
            modelBuilder.Entity<Restauran>(entity =>
            {
                entity.HasKey(e => e.RestauranID);
                entity.Property(e => e.RestauranID).HasColumnName("Restaurent");
                entity.Property(e => e.RestauranName).HasColumnName("RestaurentName");
                entity.Property(e => e.Addr).HasColumnName("Addr");
                entity.Property(e => e.Phone).HasColumnName("Phone");
            });
            
            modelBuilder.Entity<FoodRestaurent>().HasKey(sc => new { sc.FoodID, sc.RestauranID });//các khóa ngoại phải là khóa chính tổng hợp trong bảng nối
            
            modelBuilder.Entity<FoodRestaurent>()
                .HasOne<Restauran>(sc => sc.Restauran)
                .WithMany(s => s.FoodRestaurents)
                .HasForeignKey(sc => sc.RestauranID);


            modelBuilder.Entity<FoodRestaurent>()
                .HasOne<Food>(sc => sc.Food)
                .WithMany(s => s.FoodRestaurents)
                .HasForeignKey(sc => sc.FoodID);
        }
        public DbSet<Food> Foods { get; set; }
        public DbSet<Restauran> Restaurans { get; set; }
        public DbSet<FoodRestaurent> FoodRestaurents { get; set; }
        
        
    }
}