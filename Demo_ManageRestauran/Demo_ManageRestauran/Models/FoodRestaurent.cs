﻿using System.Collections.Generic;

namespace Demo_ManageRestauran.Models
{
    public class FoodRestaurent
    {
        public int FoodRestaurentID { get; set; } 
        public int FoodID { get; set; }
        public Food Food { get; set; }
        
        public int RestauranID { get; set; }
        public Restauran Restauran { get; set; }
    }
}