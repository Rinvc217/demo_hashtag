﻿using System.Collections.Generic;

namespace Demo_ManageRestauran.Models
{
    public class Food
    {
        public int FoodID { get; set; }
        public string FoodName { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        
        public virtual ICollection<FoodRestaurent> FoodRestaurents { get; set; }
    }
}