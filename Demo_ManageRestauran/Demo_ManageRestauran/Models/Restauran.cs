﻿using System.Collections.Generic;

namespace Demo_ManageRestauran.Models
{
    public class Restauran
    {
        public int RestauranID { get; set; }
        public string RestauranName { get; set; }
        public string Addr { get; set; }
        public string Phone { get; set; }
        
        public virtual ICollection<FoodRestaurent> FoodRestaurents { get; set; }
    }
}