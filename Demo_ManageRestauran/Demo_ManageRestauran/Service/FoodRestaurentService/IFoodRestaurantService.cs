﻿using System.Collections.Generic;
using Demo_ManageRestauran.Models;

namespace Demo_ManageRestauran.Service.FoodRestaurentService
{
    public interface IFoodRestaurantService
    {
        FoodRestaurent GetById(int id);
        IEnumerable<FoodRestaurent> GetAll();
        void Add(FoodRestaurent foodRestaurent);
        void Update(FoodRestaurent foodRestaurent);
        void Delete(int id);
        void Save();
    }
}