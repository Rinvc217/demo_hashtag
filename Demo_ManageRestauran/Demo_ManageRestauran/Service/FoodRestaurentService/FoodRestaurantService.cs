﻿using System.Collections.Generic;
using Demo_ManageRestauran.Models;
using Demo_ManageRestauran.UnitOfWork;

namespace Demo_ManageRestauran.Service.FoodRestaurentService
{
    public class FoodRestaurantService : IFoodRestaurantService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly DBContext _context;

        public FoodRestaurantService(IUnitOfWork unitOfWork, DBContext dbContext)
        {
            _unitOfWork = unitOfWork;
            _context = dbContext;
        }
        
        public FoodRestaurent GetById(int id)
        {
            return _unitOfWork.FoodRestaurentReponsitory.GetById(id);
        }

        public IEnumerable<FoodRestaurent>GetAll()
        {
            return _unitOfWork.FoodRestaurentReponsitory.GetAll();
        }

        public void Add(FoodRestaurent foodRestaurent)
        {
            /*

            
            
            
            */

            _unitOfWork.FoodRestaurentReponsitory.Add(foodRestaurent);
        }

        public void Update(FoodRestaurent foodRestaurent)
        {
            _unitOfWork.FoodRestaurentReponsitory.Update(foodRestaurent);
        }

        public void Delete(int id)
        {
            _unitOfWork.FoodRestaurentReponsitory.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.FoodRestaurentReponsitory.Save();
        }
        
        
    }
}