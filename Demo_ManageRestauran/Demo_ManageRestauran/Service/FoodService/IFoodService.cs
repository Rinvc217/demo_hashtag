﻿using System.Collections.Generic;
using Demo_ManageRestauran.Models;

namespace Demo_ManageRestauran.Service.FoodService
{
    public interface IFoodService
    {
        Food GetById(int id);
        IEnumerable<Food> GetAll();
        void Add(Food food);
        void Update(Food food);
        void Delete(int id);
        void Save();
    }
}