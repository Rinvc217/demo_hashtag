﻿using System.Collections.Generic;
using Demo_ManageRestauran.Models;
using Demo_ManageRestauran.UnitOfWork;

namespace Demo_ManageRestauran.Service.FoodService
{
    public class FoodService : IFoodService
    {
        
        private readonly IUnitOfWork _unitOfWork;
        private DBContext _context;

        public FoodService(IUnitOfWork unitOfWork, DBContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }
        
        public Food GetById(int id)
        {
            return _unitOfWork.FoodReponsitory.GetById(id);
        }

        public IEnumerable<Food> GetAll()
        {
            return _unitOfWork.FoodReponsitory.GetAll();
        }

        public void Add(Food food)
        {
            _unitOfWork.FoodReponsitory.Add(food);
        }

        public void Update(Food food)
        {
            _unitOfWork.FoodReponsitory.Update(food);
        }

        public void Delete(int id)
        {
            _unitOfWork.FoodReponsitory.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.FoodReponsitory.Save();
        }
    }
}