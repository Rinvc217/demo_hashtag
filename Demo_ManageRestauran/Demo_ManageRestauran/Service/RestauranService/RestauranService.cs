﻿using System.Collections.Generic;
using Demo_ManageRestauran.Models;
using Demo_ManageRestauran.UnitOfWork;

namespace Demo_ManageRestauran.Service.RestauranReponsitory
{
    public class RestauranService : IRestauranSerrvice
    {
        private readonly IUnitOfWork _unitOfWork;
        private DBContext _context;

        public RestauranService(IUnitOfWork unitOfWork, DBContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }
        
        public Restauran GetById(int id)
        {
            return _unitOfWork.RestauranReponsitory.GetById(id);
        }

        public IEnumerable<Restauran> GetAll()
        {
            return _unitOfWork.RestauranReponsitory.GetAll();
        }

        public void Add(Restauran restauran)
        {
            _unitOfWork.RestauranReponsitory.Add(restauran);
        }

        public void Update(Restauran restauran)
        {
            _unitOfWork.RestauranReponsitory.Update(restauran);
        }

        public void Delete(int id)
        {
            _unitOfWork.RestauranReponsitory.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.RestauranReponsitory.Save();
        }
    }
}