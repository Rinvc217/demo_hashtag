﻿using System.Collections.Generic;
using Demo_ManageRestauran.Models;

namespace Demo_ManageRestauran.Service.RestauranReponsitory
{
    public interface IRestauranSerrvice
    {
        Restauran GetById(int id);
        IEnumerable<Restauran> GetAll();
        void Add(Restauran restauran);
        void Update(Restauran restauran);
        void Delete(int id);
        void Save();
    }
}