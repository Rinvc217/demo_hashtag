﻿using Demo_ManageRestauran.Models;
using Demo_ManageRestauran.Reponsitory.GenericReponsitory;

namespace Demo_ManageRestauran.Reponsitory.FoodReponsitory
{
    public class FoodReponsitory : GenericReponsitory <Food>, IFoodReponsitory
    {
        public FoodReponsitory(DBContext context) : base(context)
        {
        }   
    }
}
