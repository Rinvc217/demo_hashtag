﻿using Demo_ManageRestauran.Models;
using Demo_ManageRestauran.Reponsitory.GenericReponsitory;

namespace Demo_ManageRestauran.Reponsitory.RestauranReponsitory
{
    public class RestauranReponsitory : GenericReponsitory<Restauran>, IRestauranReponsitory
    {
        public RestauranReponsitory(DBContext context) : base(context)
        {
        }
    }
}