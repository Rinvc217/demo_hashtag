﻿using Demo_ManageRestauran.Models;
using Demo_ManageRestauran.Reponsitory.GenericReponsitory;

namespace Demo_ManageRestauran.Reponsitory.FoodRestaurentReponsitory
{
    public class FoodRestaurentReponsitory : GenericReponsitory<FoodRestaurent>, IFoodRestaurentReponsitory
    {
        public FoodRestaurentReponsitory(DBContext context) : base(context)
        {
        }
    }
}