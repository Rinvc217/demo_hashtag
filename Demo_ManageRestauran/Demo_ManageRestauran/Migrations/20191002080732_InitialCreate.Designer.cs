﻿// <auto-generated />
using Demo_ManageRestauran.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Demo_ManageRestauran.Migrations
{
    [DbContext(typeof(DBContext))]
    [Migration("20191002080732_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Demo_ManageRestauran.Models.Food", b =>
                {
                    b.Property<int>("FoodID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("FoodID")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasColumnName("Description");

                    b.Property<string>("FoodName")
                        .HasColumnName("FoodName");

                    b.Property<long>("Price")
                        .HasColumnName("Price");

                    b.HasKey("FoodID");

                    b.ToTable("Foods");
                });

            modelBuilder.Entity("Demo_ManageRestauran.Models.FoodRestaurent", b =>
                {
                    b.Property<int>("FoodID");

                    b.Property<int>("RestauranID");

                    b.HasKey("FoodID", "RestauranID");

                    b.HasIndex("RestauranID");

                    b.ToTable("FoodRestaurents");
                });

            modelBuilder.Entity("Demo_ManageRestauran.Models.Restauran", b =>
                {
                    b.Property<int>("RestauranID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Restaurent")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Addr")
                        .HasColumnName("Addr");

                    b.Property<string>("Phone")
                        .HasColumnName("Phone");

                    b.Property<string>("RestauranName")
                        .HasColumnName("RestaurentName");

                    b.HasKey("RestauranID");

                    b.ToTable("Restaurans");
                });

            modelBuilder.Entity("Demo_ManageRestauran.Models.FoodRestaurent", b =>
                {
                    b.HasOne("Demo_ManageRestauran.Models.Food", "Food")
                        .WithMany("FoodRestaurents")
                        .HasForeignKey("FoodID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Demo_ManageRestauran.Models.Restauran", "Restauran")
                        .WithMany("FoodRestaurents")
                        .HasForeignKey("RestauranID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
