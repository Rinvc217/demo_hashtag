﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo_ManageRestauran.Migrations
{
    public partial class NewCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Price",
                table: "Foods",
                nullable: true,
                oldClrType: typeof(long));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "Price",
                table: "Foods",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
