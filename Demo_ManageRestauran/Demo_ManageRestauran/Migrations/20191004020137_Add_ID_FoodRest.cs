﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo_ManageRestauran.Migrations
{
    public partial class Add_ID_FoodRest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FoodRestaurentID",
                table: "FoodRestaurents",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FoodRestaurentID",
                table: "FoodRestaurents");
        }
    }
}
