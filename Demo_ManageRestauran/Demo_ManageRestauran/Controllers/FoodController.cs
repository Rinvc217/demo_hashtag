﻿using Demo_ManageRestauran.Models;
using Demo_ManageRestauran.Service.FoodService;
using Microsoft.AspNetCore.Mvc;

namespace Demo_ManageRestauran.Controllers
{
    public class FoodController : Controller
    {
        private IFoodService _FoodService;

        public FoodController(IFoodService classService)
        {
            _FoodService = classService;
        }
        
        // Get:Class/
        [HttpGet]
        public IActionResult Index()
        {
            return View(_FoodService.GetAll());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Food food)
        {
            _FoodService.Add(food);
            _FoodService.Save();
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public IActionResult Edit(int id)
        {
            var getId = _FoodService.GetById(id);
            return View(getId);
        }
        
        [HttpPost]
        public IActionResult Edit(Food food)
        {
            _FoodService.Update(food);
            _FoodService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _FoodService.Delete(id);
            _FoodService.Save();
            return RedirectToAction("Index");
        }
    }
    
}