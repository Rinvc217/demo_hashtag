﻿using Demo_ManageRestauran.Models;
using Demo_ManageRestauran.Service.RestauranReponsitory;
using Microsoft.AspNetCore.Mvc;

namespace Demo_ManageRestauran.Controllers
{
    public class RestauranController : Controller
    {
        private IRestauranSerrvice _restauranSerrvice;

        public RestauranController(IRestauranSerrvice restauranSerrvice)
        {
            _restauranSerrvice = restauranSerrvice;
        }

        // GET
        public IActionResult Index()
        {
            return View(_restauranSerrvice.GetAll());
        }
        
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult Create(Restauran restauran)
        {
            _restauranSerrvice.Add(restauran);
            _restauranSerrvice.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var getId = _restauranSerrvice.GetById(id);
            return View(getId);
        }

        [HttpPost]
        public IActionResult Edit(Restauran restauran)
        {
            _restauranSerrvice.Update(restauran);
            _restauranSerrvice.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _restauranSerrvice.Delete(id);
            _restauranSerrvice.Save();
            return RedirectToAction("Index");
        }
    }
}