﻿using Demo_ManageRestauran.Service.FoodRestaurentService;
using Microsoft.AspNetCore.Mvc;

namespace Demo_ManageRestauran.Controllers
{
    public class FoodRestaurentController : Controller
    {
        private readonly IFoodRestaurantService _foodRestaurantService;

        public FoodRestaurentController(IFoodRestaurantService foodRestaurantService)
        {
            _foodRestaurantService = foodRestaurantService;
        }
        // GET
        [HttpGet]
        public IActionResult Index()
        {
            return View(_foodRestaurantService.GetAll());
        }
        
    }
}